﻿using System;

namespace Factorial
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a number:");
            int number = 0;
            bool parse = int.TryParse(Console.ReadLine(), out number);
            if (parse)
            {
                if(number < 0)
                {
                    Console.WriteLine("A number greate or equal to zero is required");
                }
                else
                {
                    Console.WriteLine("Factorial of {0} is {1}", number, getFactorial(number));

                    Console.ReadLine();
                }
                
            }
        }

        public static int getFactorial(int number)
        {
            if(number < 2)
            {
                return 1;
            }
            else
            {
                int fact = 1;
                for(int i = number; i > 0; i--)
                {
                    fact *= i;
                }
                return fact;
            }
        }
    }
}
